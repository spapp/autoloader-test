<?php

namespace A;

class A {
    public function __construct() {
        echo '__construct ' . __CLASS__ . PHP_EOL;
    }
}