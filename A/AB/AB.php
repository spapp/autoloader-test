<?php

namespace A\AB;

class AB {
    public function __construct() {
        echo '__construct ' . __CLASS__ . PHP_EOL;
    }
}