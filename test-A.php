<?php

spl_autoload_register(function ($className) {
    echo 'autload A: ' . $className . PHP_EOL;

    require_once __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
});

spl_autoload_register(function ($className) {
    echo 'autload B: ' . $className . PHP_EOL;

    require_once __DIR__ . '/' . strtolower(str_replace('\\', '/', $className)) . '.class.php';
});

new \A\A();
new \A\AA\AA();
new \A\AB\AB();

new \inc\A();
new \inc\B();
