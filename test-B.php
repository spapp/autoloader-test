<?php

// https://secure.php.net/manual/en/function.spl-autoload-register.php
// https://secure.php.net/manual/en/function.spl-autoload.php
//
// not important the return value
// but important to checks that the autoloader can load the class
// because the autoload logic fits only one file structure
// if we check it we can avoid some errors
spl_autoload_register(function ($className) {
    echo 'autload A: ' . $className;

    $needsHandle = 0 === strpos($className, 'A');

    if ($needsHandle) {
        require_once __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';

        echo ' - handled';
    }

    echo PHP_EOL;

    return true;
});

spl_autoload_register(function ($className) {
    echo 'autload B: ' . $className;

    $needsHandle = 0 === strpos($className, 'inc');

    if ($needsHandle) {
        require_once __DIR__ . '/' . strtolower(str_replace('\\', '/', $className)) . '.class.php';

        echo ' - handled';
    }

    echo PHP_EOL;
});


new \A\A();
new \A\AA\AA();
new \A\AB\AB();

new \inc\A();
new \inc\B();
